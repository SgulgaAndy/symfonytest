<?php

namespace App\Controller;

use App\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;


class CsvController extends AbstractController
{
    /**
     * @Route("/csv/{id}", name="csv")
     * @ParamConverter("post", class="App:Product")
     *
     * @param SerializerInterface $serializer
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function index(SerializerInterface $serializer, Product $product)
    {
        $serializer = $this->container->get('serializer');

        $data = $serializer->decode(file_get_contents('/var/www/testwork.loc/public/uploads/brochures/'.$product->getBrochureFilename()), 'csv');

        $i = 0;
        foreach ($data as $d)
        {
            $errors = array();
            if(preg_match('/[^0-9]/', $d['internalInvoiceId']))
            {
                $errors[] = 'File has an error (INTERNAL INVOICE ID: '.$d['internalInvoiceId'].'). Only digits!';
            }

            $date1 = new \DateTime($d['dueOn']);
            $date2 = new \DateTime('now');

            $date2->modify('-30 day');
            if ($date1->format('Y-m-d') <= $date2->format('Y-m-d'))
            {
                $display = '0.5';
            }
            else
            {
                $display = '0.3';
            }
            $data[$i]['display'] = $display;
            $i++;
        }

        return $this->render('csv/index.html.twig', [
            'controller_name' => 'CsvController',
            'data' => $data,
            'errors' => $errors,
        ]);
    }
}
