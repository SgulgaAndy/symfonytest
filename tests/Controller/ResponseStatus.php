<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ResponseStatus extends WebTestCase
{
    public function testShowPost()
    {
        $client = static::createClient();

        $filePath = '/var/www/testwork.loc/public/uploads/brochures/file_valid-5ec24e6ff279f.txt';

        $csv = new UploadedFile(
            $filePath,
            'file_valid-5ec3f39f6903e.txt',
            'text/plain',
            null
        );

        $client->request(
            'POST',
            '/',
            array('product_brochure' => $csv)
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertFileExists($filePath);
        $this->assertFileEquals($filePath, $filePath);
        $this->assertFileIsReadable($filePath);
    }
}